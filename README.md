
## Quick Start

1.  Clone the repo `git clone https://gitlab.com/chandru_pwc/depo_admin.git`
2.  Go to your project folder from your terminal
3.  Run: `npm install` or `yarn install`
4.  After install, run: `npm run start` or `yarn start`
5.  It will open your browser(http://localhost:3000)

## Build

1. Build the application `npm run build`
2. Host the application `npm run serve`
3. Firebase Login  `firebase login`
4. Firebase Deploying `firebase deploy`    