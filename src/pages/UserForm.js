import React, { useState } from 'react';
// import './DepoUserForm.css'
import querystring from 'querystring';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  UncontrolledAlert,
} from 'reactstrap';
// import { useStateValue } from '../components/utility/stateProvider';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function UserForm(props) {
  const [loginusertype, setLoginUserType] = useState(
    localStorage.getItem('userType'),
  );
  const [depocode, setDepocode] = useState(localStorage.getItem('depocode'));
  const [userToken, setUserToken] = useState(
    localStorage.getItem('user_token'),
  );
  const [success, setSuccess] = useState('');
  const [iserror, setIsError] = useState('');
  const [isconnerror, setIsConnError] = useState('');
  const [usertype, setUsertype] = useState('depouser');
  const [username, setUsername] = useState('');
  const [userfname, setUserFName] = useState('');
  const [userlname, setUserLName] = useState('');
  const [useremail, setUserEmail] = useState('');
  const [usermblno, setUserMblNo] = useState('');
  const [userpass, setUserPass] = useState('');
  const [custname, setCustName] = useState('');
  const [usercustname, setUserCustName] = useState('');

  const userformSubmitHandler = e => {
    console.log(depocode);
    e.preventDefault();
    if (
      usertype === '' ||
      username === '' ||
      userfname === '' ||
      userlname === '' ||
      useremail === '' ||
      usermblno === '' ||
      userpass === '' 
      // custname === '' ||
      // usercustname === ''
    ) {
      setIsError('errors');
      return;
    } else {
      axios
        .post(
          'https://ipix.karsha.co.in/user/register',
          querystring.stringify({
            usertype: usertype,
            username: username,
            userfname: userfname,
            userlname: userlname,
            useremail: useremail,
            usermobno: usermblno,
            password: userpass,
            custname: custname,
            usercustname: usercustname,
            depocode: depocode,
            role: 'superadmin',
          }),
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              sessiontoken: userToken,
            },
          },
        )
        .then(data => {
          // setLoadedData(data.data.results);
          console.log(data);
          alert('User Registered Successfully');
          setSuccess('success');
          if (loginusertype === 'customer') {
            props.history.push('/customeruserspage');
          } else {
            props.history.push('/users');
          }
        })
        .catch(error => {
          setIsConnError('errors');
          console.log('erorrr');
        });
    }
  };

  const ErrorMessage = (
    <UncontrolledAlert color="danger">
      Please enter all the fields
    </UncontrolledAlert>
  );

  const ConnErrorMessage = (
    <UncontrolledAlert color="danger">
      Connection Failed, Please Try Again Later
    </UncontrolledAlert>
  );

  const SuccessMessage = (
    <UncontrolledAlert color="danger">
      User Registered Successfully
    </UncontrolledAlert>
  );

  return (
    <div className="container">
      <h2 className="text-center">User Creation Form</h2>
      <Form>
        <FormGroup>
          <Label htmlFor="usertype">Usertype :</Label>
          {loginusertype === 'customer' ? (
            <div className="col-6">
               {' '}
              <Input
                type="radio"
                id="depoadmin"
                name="depoadmin"
                value="depoadmin"
                onChange={e => setUsertype(e.target.value)}
              />
                <Label htmlFor="depoadmin">Depo Admin</Label>
            </div>
          ) : null}
          <div className="col-6">
             {' '}
            <Input
              type="radio"
              id="depoUser"
              name="depoUser"
              value="depouser"
              onChange={e => setUsertype(e.target.value)}
            />
              <Label htmlFor="depoUser">Depo User</Label>
          </div>
          <div className="col-6">
             {' '}
            <Input
              type="radio"
              id="depoSurveyor"
              name="depoSurveyor"
              value="deposurveyor"
              onChange={e => setUsertype(e.target.value)}
            />
              <Label htmlFor="depoSurveyor">Depo Surveyor</Label>
          </div>
        </FormGroup>
        <FormGroup>
          <Label htmlFor="username">Username:</Label>
          <Input
            type="text"
            className="form-control"
            id="username"
            placeholder="Enter Username"
            name="username"
            onChange={e => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="userfname">User Firstname:</Label>
          <Input
            type="text"
            className="form-control"
            id="userfname"
            placeholder="Enter User Firstname"
            name="userfname"
            onChange={e => setUserFName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="userlname">User Lastname:</Label>
          <Input
            type="text"
            className="form-control"
            id="userlname"
            placeholder="Enter User Lastname"
            name="userlname"
            onChange={e => setUserLName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="email">Email:</Label>
          <Input
            type="email"
            className="form-control"
            id="email"
            placeholder="Enter E-mail"
            name="email"
            onChange={e => setUserEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="mblnumber">User Mobilenumber:</Label>
          <Input
            type="number"
            className="form-control"
            id="mblnumber"
            placeholder="Enter User Mobilenumber"
            name="mblnumber"
            onChange={e => setUserMblNo(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="pwd">Password:</Label>
          <Input
            type="password"
            className="form-control"
            id="pwd"
            placeholder="Enter password"
            name="pwd"
            onChange={e => setUserPass(e.target.value)}
          />
        </FormGroup>
        {/* <FormGroup>
          <Label htmlFor="custname">Customer Name:</Label>
          <Input
            type="text"
            className="form-control"
            id="custname"
            placeholder="Enter Customer Name"
            name="custname"
            onChange={e => setCustName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="usercustname">User Customername:</Label>
          <Input
            type="text"
            className="form-control"
            id="usercustname"
            placeholder="Enter User Customername"
            name="usercustname"
            onChange={e => setUserCustName(e.target.value)}
          />
        </FormGroup> */}
        <br />
        <div className="form-group mb-3 text-center">
          {iserror !== '' ? ErrorMessage : null}
        </div>
        <div className="form-group mb-3 text-center">
          {isconnerror !== '' ? ConnErrorMessage : null}
        </div>
        <div className="form-group mb-3 text-center">
          {success !== '' ? SuccessMessage : null}
        </div>
        <div className="row">
          <div className="col-md-2">
            {loginusertype === 'customer' ? (
              <div>
                <Link to="/customeruserspage">
                  <Button color="primary">Back</Button>
                </Link>
              </div>
            ) : (
              <div>
                {' '}
                <Link to="/users">
                  <Button color="primary">Back</Button>
                </Link>{' '}
              </div>
            )}
          </div>
          <div className="offset-8 col-md-2">
            <Button
              type="submit"
              color="primary"
              style={{ float: 'right' }}
              onClick={userformSubmitHandler}
            >
              Submit
            </Button>
          </div>
        </div>
      </Form>
      <ToastContainer position="top-center" />
    </div>
  );
}

export default UserForm;
