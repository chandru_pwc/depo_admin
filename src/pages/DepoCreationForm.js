import React, { useState } from 'react';
import {
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  UncontrolledAlert,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import querystring from 'querystring';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function DepoCreationForm(props) {
  const [userToken, setUserToken] = useState(
    localStorage.getItem('user_token'),
  );
  const [success, setSuccess] = useState('');
  const [iserror, setIsError] = useState('');
  const [isconnerror, setIsConnError] = useState('');
  const [depo_code, setDepocode] = useState('');
  const [depo_name, setDeponame] = useState('');
  const [depo_desc, setDepodesc] = useState('');

  const depoCreate = e => {
    e.preventDefault();
    if (depo_code === '' || depo_name === '' || depo_desc === '') {
      setIsError('errors');
      return;
    } else {
      axios
        .post(
          'https://ipix.karsha.co.in/depo/depocreation',
          querystring.stringify({
            depocode: depo_code,
            deponame: depo_name,
            depodesc: depo_desc,
            depoleasingcompany: 'seashore leasing company',
            depolat: '13.067439',
            depolon: '80.237617',
          }),
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              sessiontoken: userToken,
            },
          },
        )
        .then(data => {
          // setLoadedData(data.data.results);
          alert('Depo created succesfully');
          setSuccess('success');
          props.history.push('/customerdepopage');
        })
        .catch(error => {
          setIsConnError('errors');
        });
    }
  };

  const ErrorMessage = (
    <UncontrolledAlert color="danger">
      Please enter all the fields
    </UncontrolledAlert>
  );

  const ConnErrorMessage = (
    <UncontrolledAlert color="danger">
      Connection Failed, Please Try Again Later
    </UncontrolledAlert>
  );

  const SuccessMessage = (
    <UncontrolledAlert color="danger">
      Depo created succesfully
    </UncontrolledAlert>
  );

  return (
    <div className="container">
      <h2 className="text-center">Depo Creation Form</h2>
      <Form>
        <FormGroup>
          <Label htmlFor="deponame">Depo Name</Label>
          <Input
            type="text"
            className="form-control"
            id="deponame"
            placeholder="Enter Depo Name"
            name="deponame"
            onChange={e => setDepocode(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="depocode">Depo Code</Label>
          <Input
            type="text"
            className="form-control"
            id="depocode"
            placeholder="Enter Depo Code"
            name="depocode"
            onChange={e => setDeponame(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label htmlFor="depodesc">Depo Description</Label>
          <Input
            type="text"
            className="form-control"
            id="depodesc"
            placeholder="Enter Depo Description"
            name="depodesc"
            onChange={e => setDepodesc(e.target.value)}
          />
        </FormGroup>
        <br />
        <div className="form-group mb-3 text-center">
          {iserror !== '' ? ErrorMessage : null}
        </div>
        <div className="form-group mb-3 text-center">
          {isconnerror !== '' ? ConnErrorMessage : null}
        </div>
        {/* <div className="form-group mb-3 text-center">
          {success !== '' ? SuccessMessage : null}
        </div> */}
        <div className="row">
          <div className="col-md-2">
            <Link to="/customerdepopage">
              <Button color="primary">Back</Button>
            </Link>
          </div>
          <div className="offset-8 col-md-2">
            <Button
              type="submit"
              color="primary"
              style={{ float: 'right' }}
              onClick={depoCreate}
            >
              Create
            </Button>
          </div>
        </div>
      </Form>
      <ToastContainer position="top-center" />
    </div>
  );
}
export default DepoCreationForm;
