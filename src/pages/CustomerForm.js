import React, { useState } from 'react';
import {
  Button,
  Form,
  FormGroup,
  Input,
  Label,
  UncontrolledAlert,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import querystring from 'querystring';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function CustomerForm(props) {
  const [userToken, setUserToken] = useState(
    localStorage.getItem('user_token'),
  );
  const [iserror, setIsError] = useState('');
  const [isconnerror, setIsConnError] = useState('');
  const [success, setSuccess] = useState('');
  const [customername, setCustomerName] = useState('');
  const [username, setUserName] = useState('');
  const [customermblno, setCustomerMblno] = useState('');
  const [customeremail, setCustomerEmail] = useState('');
  const [customeraddress, setCustomerAddress] = useState('');

  const handleCustomerSubmit = e => {
    e.preventDefault();
    if (
      customername === '' ||
      username === '' ||
      customermblno === '' ||
      customeremail === '' ||
      customeraddress === ''
    ) {
      setIsError('errors');
      return;
    } else {
      axios
        .post(
          'https://ipix.karsha.co.in/user/register',
          querystring.stringify({
            role: 'superadmin',
            usertype: 'customer',
            custname: customername,
            custusername: username,
            custmobno: customermblno,
            custemail: customeremail,
            password: 'test@123',
            custadd: customeraddress,
            custcity: 'Chennai',
            custstate: 'Tamilnadu',
            custcountry: 'India',
            custlat: '13.067439',
            custlon: '80.237617',
            depocode: 'CHA_TRI',
          }),
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              sessiontoken: userToken,
            },
          },
        )
        .then(data => {
          // setLoadedData(data.data.results);
          alert('Customer created successfully');
          // setSuccess('success');
          props.history.push('/customerpage');
        })
        .catch(error => {
          setIsConnError('errors');
        });
    }
  };

  const ErrorMessage = (
    <UncontrolledAlert color="danger">
      Please enter all the fields
    </UncontrolledAlert>
  );

  const ConnErrorMessage = (
    <UncontrolledAlert color="danger">
      Connection Failed, Please Try Again Later
    </UncontrolledAlert>
  );

  const SuccessMessage = (
    <UncontrolledAlert color="danger">
      Customer created successfully
    </UncontrolledAlert>
  );

  return (
    <div className="container">
      <h2 className="text-center">Customer Creation Form</h2>
      <Form>
        <FormGroup>
          <Label for="customername">Customer Name</Label>
          <Input
            type="text"
            name="customername"
            placeholder="Enter Customer Name"
            onChange={e => setCustomerName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="mobilenumber">Mobile Number</Label>
          <Input
            type="number"
            name="mobilenumber"
            placeholder="Enter Mobile Number"
            onChange={e => setCustomerMblno(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input
            type="email"
            name="email"
            placeholder="Enter E-mail"
            onChange={e => setCustomerEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="contactperson">Contact Person</Label>
          <Input
            type="text"
            name="contactperson"
            placeholder="Enter Contact Person"
            onChange={e => setUserName(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="address">Address</Label>
          <Input
            type="text"
            name="address"
            placeholder="Enter Address"
            onChange={e => setCustomerAddress(e.target.value)}
          />
        </FormGroup>

        <div className="form-group mb-3 text-center">
          {iserror !== '' ? ErrorMessage : null}
        </div>
        <div className="form-group mb-3 text-center">
          {isconnerror !== '' ? ConnErrorMessage : null}
        </div>
        <div className="form-group mb-3 text-center">
          {success !== '' ? SuccessMessage : null}
        </div>

        <div className="row">
          <div className="col-md-2">
            <Link to="/customerpage">
              <Button color="primary">Back</Button>
            </Link>
          </div>
          <div className="offset-8 col-md-2">
            <Button
              type="submit"
              color="primary"
              style={{ float: 'right' }}
              onClick={handleCustomerSubmit}
            >
              Submit
            </Button>
          </div>
        </div>
      </Form>
      <ToastContainer position="top-center" />
    </div>
  );
}

export default CustomerForm;
