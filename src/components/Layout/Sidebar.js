// import logo200Image from '../../assets/img/logo/logo_200.png';
import sidebarBgImage from '../../assets/img/sidebar/sidebar-4.jpg';
import SourceLink from '../../components/SourceLink';
import React, { useState } from 'react';
// import { FaGithub } from 'react-icons/fa';
import { BiAnchor } from 'react-icons/bi';
import { ImUsers } from 'react-icons/im';
// import {
//   MdAccountCircle,
//   MdArrowDropDownCircle,
//   MdBorderAll,
//   MdBrush,
//   MdChromeReaderMode,
//   MdDashboard,
//   MdExtension,
//   MdGroupWork,
//   MdInsertChart,
//   MdKeyboardArrowDown,
//   MdNotificationsActive,
//   MdPages,
//   MdRadioButtonChecked,
//   MdSend,
//   MdStar,
//   MdTextFields,
//   MdViewCarousel,
//   MdViewDay,
//   MdViewList,
//   MdWeb,
//   MdWidgets,
// } from 'react-icons/md';
// import {BiLogOut, BiPackage, BiUserDetail} from 'react-icons/bi';
import { IoMdCube } from 'react-icons/io';
import { FaHome, FaUsers, FaAddressCard, FaUserCircle } from 'react-icons/fa';
// import {CgUserList} from 'react-icons/cg'
import { NavLink } from 'react-router-dom';
import {
  // UncontrolledTooltip,
  // Collapse,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink,
} from 'reactstrap';
import bn from '../../utils/bemnames';
// import { useStateValue } from '../utility/stateProvider';

const sidebarBackground = {
  // backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

const bem = bn.create('sidebar');

const Sidebar = props => {
  // const [isOpenComponents, setIsOpenComponents] = useState(true);
  // const [isOpenContents, setIsOpenContents] = useState(true);
  // const [isOpenPages, setIsOpenPages] = useState(true);

  // const [{email}, dispatch] = useStateValue();

  const [usertype, setUserType] = useState(localStorage.getItem('userType'));
  let navItems = [];
  if (
    usertype === 'depoadmin' ||
    usertype === 'deposurveyor'
  ) {
    navItems = [
      { to: '/home', name: 'home', exact: true, Icon: FaHome },
      { to: '/containers', name: 'containers', exact: false, Icon: IoMdCube },
      { to: '/users', name: 'users', exact: false, Icon: FaUsers },
      {
        to: '/containerrequest',
        name: 'request',
        exact: false,
        Icon: FaAddressCard,
      },
      { to: '/myaccount', name: 'my account', exact: false, Icon: FaUserCircle },
    ];
  } else if (usertype === 'customer') {
    navItems = [
      { to: '/customerhomepage', name: 'home', exact: true, Icon: FaHome },
      {
        to: '/customerrequest',
        name: 'request',
        exact: false,
        Icon: FaAddressCard,
      },
      { to: '/customeruserspage', name: 'users', exact: false, Icon: FaUsers },
      { to: '/customerdepopage', name: 'depo', exact: false, Icon: BiAnchor },
      { to: '/customerpage', name: 'customers', exact: false, Icon: ImUsers },
    ];
  } else if( usertype === 'depoUser'){
    // alert("Please fill all the fields")
    // props.history.push("/")
    navItems = [
      { to: '/home', name: 'home', exact: true, Icon: FaHome },
      { to: '/containers', name: 'containers', exact: false, Icon: IoMdCube },
      // { to: '/users', name: 'users', exact: false, Icon: FaUsers },
      {
        to: '/containerrequest',
        name: 'request',
        exact: false,
        Icon: FaAddressCard,
      },
      { to: '/myaccount', name: 'my account', exact: false, Icon: FaUserCircle },
    ];
  }
  const handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];

      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  return (
    <aside className={bem.b()} data-image={sidebarBgImage}>
      <div className={bem.e('background')} style={sidebarBackground} />
      <div className={bem.e('content')}>
        <Navbar>
          <SourceLink className="navbar-brand d-flex">
            {/* <img
                src={logo200Image}
                width="40"
                height="30"
                className="pr-2"
                alt=""
              /> */}
            <span className="text-white">
              {/* {usertype === 'depoadmin' ||
              usertype === 'depoUser' ||
              usertype === 'deposurveyor'
                ? 'Depo Admin'
                : 'Customer Admin'} */}
                {
                  usertype === 'depoadmin' ? 'Depo Admin' : 
                    usertype === 'depoUser' ? 'Depo User' : 'Customer Admin'
                  
                }

              {/* <FaGithub /> */}
            </span>
          </SourceLink>
        </Navbar>
        <Nav vertical>
          {navItems.map(({ to, name, exact, Icon }, index) => (
            <NavItem key={index} className={bem.e('nav-item')}>
              <BSNavLink
                id={`navItem-${name}-${index}`}
                className="text-uppercase"
                tag={NavLink}
                to={to}
                activeClassName="active"
                exact={exact}
              >
                <Icon className={bem.e('nav-item-icon')} />
                <span className="">{name}</span>
              </BSNavLink>
            </NavItem>
          ))}
        </Nav>
      </div>
    </aside>
  );
};

export default Sidebar;

{
  /* 
            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Components')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdExtension className={bem.e('nav-item-icon')} />
                  <span className=" align-self-start">Components</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenComponents
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem> */
}
{
  /* <Collapse isOpen={this.state.isOpenComponents}>
              {navComponents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse> */
}
{
  /* 
            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Contents')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdSend className={bem.e('nav-item-icon')} />
                  <span className="">Contents</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenContents
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem> */
}
{
  /* <Collapse isOpen={this.state.isOpenContents}>
              {navContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse> */
}

{
  /* <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Pages')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdPages className={bem.e('nav-item-icon')} />
                  <span className="">Pages</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenPages
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem> */
}
{
  /* <Collapse isOpen={this.state.isOpenPages}>
              {pageContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    className="text-uppercase"
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse> */
}
